<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kmyapplication "<application
>Sam</application
>">
  <!ENTITY kappname "&kmyapplication;">
  <!ENTITY package "kde-module">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE">

]>
<book id="sam" lang="&language;">

<bookinfo>

<othercredit role="translator"
><firstname
>André Marcelo</firstname
><surname
>Alvarenga</surname
><affiliation
><address
><email
>alvarenga@kde.org</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
> 


<date
>13/12/2012</date>
<releaseinfo
>0.4</releaseinfo>

<abstract>
<para
>O &kmyapplication; é um gerenciador de modelos acústicos. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>Kapp</keyword>
<keyword
>Simon</keyword>
<keyword
>reconhecimento</keyword>
<keyword
>fala</keyword>
<keyword
>voz</keyword>
<keyword
>comando</keyword>
<keyword
>controle</keyword>
<keyword
>modelo</keyword>
<keyword
>compilação</keyword>
<keyword
>Sam</keyword>
<keyword
>acessibilidade</keyword>
</keywordset>

<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2009-2012</year>
<holder
>Peter Grasch</holder>
</copyright>

<authorgroup>
<author
><personname
> <firstname
>Peter</firstname
> <othername
>H.</othername
> <surname
>Grasch</surname
> </personname
> <email
>peter.grasch@bedahr.org</email
> </author>
</authorgroup>


<title
>Manual do &kmyapplication;</title>
</bookinfo>

<chapter id="introduction">
<title
>Introdução</title>

<para
>O &kmyapplication; é uma ferramenta para criar e testar modelos acústicos. Ele pode compilar novos modelos de fala, usar os modelos criados pelo Simon e produzir modelos que podem ser usados posteriormente pelo Simon.</para>
<para
>Ele é destinado a pessoas que desejam mais controle sobre o seu modelo acústico e oferece um acesso de nível mais baixo ao processo de compilação. O &kmyapplication; é destinado principalmente para os profissionais da fala que desejam melhorar e/ou testar o seus modelos acústicos.</para>
<para
>Para mais informações sobre a arquitetura do pacote Simon, verifique o <ulink url="help:/simon/overview.html#architecture"
>Manual</ulink
>.</para>

<sect1 id="background">
  <title
>Plano de fundo</title>

  <para
>Esta seção fornecerá alguma informação de fundo sobre o processo de compilação e testes.</para>

<sect2 id="effective_testing">
  <title
>Testes efetivos</title>
  <para
>Uma das funcionalidades mais importantes do Sam é testar os modelos acústicos gerados.</para>

  <para
>O procedimento de testes básico é executar o reconhecimento sobre as amostras, onde a transcrição já é conhecida e compar os resultados. O &kmyapplication; também recebe o valor de confiança do reconhecimento em conta para medir a robustez do sistema criado.</para>

  <para
>Devido à forma como são criados os modelos acústicos, tanto a precisão do reconhecimento quanto a confiança serão altamente desviados quando as mesmas amostras são usadas para testes e treinamento. Isto é chamado de testes "de conteúdo" (as amostras usadas para testes também estão no seu conteúdo de treinamento).</para>

  <para
>Embora o teste de conteúdo possa indicar se o processo de compilação falhou ou produziu resultados pouco satisfatórios, não será informado qual a taxa de reconhecimento "real" do modelo criado. Como tal, recomenda-se fazer um teste "sem conteúdo": usar amostras diferentes para o treinamento e para os testes.</para>
  <para
>Para os testes fora de conteúdo, basta dividir o seu arquivo de mensagens em duas partes: uma usada para compilar o modelo e outra para testá-lo. Obviamente, o conjunto de testes não precisa ser muito grande para obter um resultado representativo.</para>
  <para
>Se você não tiver muitos dados para treinamento, poderá também dividir o conteúdo completo em dez partes. Compile 10 modelos, onde cada um trata de uma parte do conteúdo. Depois faça testes individuais (sempre com o conjunto de testes que foi excluído durante a compilação) e uma média dos resultados.</para>

</sect2>
</sect1>

</chapter>

<chapter id="using-kapp">
<title
>Usando o &kmyapplication;</title>

<para
>O &kmyapplication; oferece uma interface gráfica dividida em cinco áreas.</para>

<para>
<screenshot>
<screeninfo
>Janela principal</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="main.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Janela principal</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

<para
>A ordem das abas representa um fluxo de trabalho completo do Sam, desde a criação de um modelo para testá-lo. </para>

<sect1 id="input_output">
  <title
>Arquivos de entrada e saída</title>

  <para
>Defina os arquivos com que deseja trabalhar. Você pode salvar e carregar esta configuração usando os botões <guibutton
>Salvar</guibutton
> e <guibutton
>Carregar</guibutton
>, respectivamente.</para>
<warning>
    <para
>Os menus <guibutton
>Salvar</guibutton
> e <guibutton
>Carregar</guibutton
> <emphasis
>apenas</emphasis
> salvam os locais e opções aqui definidas. Eles não salvam os arquivos associados!</para>
</warning>

<para
>Aqui você tem a opção para selecionar as infraestruturas de compilação/teste do modelo. Dependendo da sua instalação do Simon, nem todas poderão estar disponíveis.</para>

<para>
<screenshot>
<screeninfo
>Seleção de entrada e saída</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="input_output.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Seleção de entrada e saída</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

<para
>Se quiser compilar e/ou testar o modelo do Simon, você pode usar a opção <guibutton
>Modificar o modelo do Simon</guibutton
> para carregar os arquivos apropriados. Ambos os cenários e mensagens serão serializados de forma apropriada.</para>

<para
>Quando o Simon compila o modelo, ele irá remover automaticamente as palavras não treinadas durante a formatação, assim como vai adaptando as mensagens de forma apropriada. Isto também acontece ao selecionar a opção <guibutton
>Modificar o modelo do Simon</guibutton
>. Se no entanto, você formatá-los separadamente com as opções <guibutton
>Formatar os cenários</guibutton
> e <guibutton
>Formatar as mensagens</guibutton
>, esta adaptação não será efetuada e você será o responsável pela validação dos arquivos de entrada. Se você indicar diretamente os nomes dos arquivos, isto ainda se aplicará com maior certeza.</para>

<para
>Ao selecionar um modelo estático como tipo de modelo usado, o &kmyapplication; ainda irá compilar o modelo do idioma, tal como aconteceria com o Simon. Ele também copia os arquivos de entrada do modelo de base para os locais de saída.</para>

<para
>O &kmyapplication; permite ao usuário fornecer tipos diferentes de arquivos de mensagens para criação e teste do modelo. Para mais informações sobre o uso efetivo deste recurso, veja a seção de <link linkend="effective_testing"
>testes efetivos</link
>. </para>

<para>
<screenshot>
<screeninfo
>Configuração do teste</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="sam_test_io.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Configuração do teste</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

<para
>Os usuários podem configurar um número arbitrário de configurações de teste para comparar diferentes parâmetros do modelo. Todos os testes configurados serão executados e apresentados nos relatórios que podem ser exportados. Através da opção para indicar arquivos de entrada diferentes do modelo compilado, podem ser incluídos modelos de referência (ou de base) no relatório. </para>

<para
>A partir do Simon 0.4, o formato de arquivo primário dos modelos é o novo formato SBM. Embora o Sam também irá criar modelos SBM, o teste ainda necessita dos arquivos de baixo nível. Isto pode ser contra-intuitivo, mas apresenta algumas vantagens práticas: A compilação do modelo do Simon (e, como tal, da compilação do modelo SAM) continua a gerar arquivos de baixo nível para os compiladores de modelos SPHINX e HTK antes de incluí-los em um pacote SBM, não sendo muito difícil indicar o uso destes arquivos (temporários) pelo SAM. Este é o comportamento normal ao usar a opção <guibutton
>Modificar o modelo do Simon</guibutton
>. Como a compilação do modelo acústico é normalmente um processo repetitivo, o compilador produz normalmente várias versões temporárias das diferentes etapas da estimativa dos parâmetros. Ao permitir dados de entrada de baixo nível, os testes e comparações destes resultados intermediários são muito mais simples. </para>
<para
>Se quiser testar um modelo que tenha recebido como um contêiner SBM, você pode extraí-lo com o SAM através da opção <guibutton
>Extrair modelo do Simon</guibutton
> para ganhar acesso aos arquivos de baixo nível que são necessários para testar com o &kmyapplication;. </para>

</sect1>

<sect1 id="conservative_training">
<title
>Treinamento conservador</title>
<para
>Se você <ulink url="help:/simond/using-kapp.html#User_Configuration"
>configurou o Simond para manter as amostras de reconhecimento</ulink
>, o Sam poderá importar estes dados através da opção <guibutton
>Importar as amostras de reconhecimento</guibutton
> na seção "Entrada e saída".</para>

<para>
<screenshot>
<screeninfo
>Treinamento conservador</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="conservative_training.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Treinamento conservador</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

<para
>A legenda do arquivo de mensagens será construída automaticamente a partir do resultado do reconhecimento mais provável. Você pode limitar a seleção para incluir apenas as amostras que atingiram uma determinada pontuação de confiança.</para>

<para
>Se a configuração do Simon de onde você está importando as amostras usar um <ulink url="help:/simon/commands.html#filter_command_plugin"
>filtro global para desativar o reconhecimento</ulink
>, então é possível fornecer ao Sam o comando que ativa e desativa este filtro (pausar e prosseguir o reconhecimento, respectivamente) para ignorar as amostras que forem produzidas enquanto o reconhecimento estiver desativado.</para>
</sect1>

<sect1 id="adapt_scenarios">
  <title
>Adaptar os cenários</title>

  <para
>O Simon armazena o modelo do idioma em cenários. Quando você quiser usá-los para compilar o modelo, precisa primeiro formatá-los para que se tornem legíveis pelo SPHINX ou Julius e pelo HTK.</para>

<para>
<screenshot>
<screeninfo
>Adaptar os cenários</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="adapt_scenarios.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Adaptar os cenários</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

  <para
>Para fazer isto, selecione o botão <guibutton
>Formatar os cenários</guibutton
> na aba <link linkend="input_output"
>Entrada e saída</link
>. Os cenários do Simon também serão formatados ao usar as opções para <guibutton
>Modificar o modelo do Simon</guibutton
>.</para>

  <para
>Nesta página você pode encontrar a informação de status da adaptação e ler as mensagens de erro detalhadas, caso ocorra algum erro.</para>
</sect1>

<sect1 id="build_model">
  <title
>Criar o modelo</title>

  <para
>Aqui você pode compilar o modelo usando os arquivos de entrada e saída definidos na <link linkend="input_output"
>seção de entrada e saída</link
>.</para>

  <para
>O processo de compilação é idêntico ao usado pelo Simond. Entretanto, ao contrário do Simond, o registro completo com todos os programas externos carregados, o seu resultado, assim como a informação de progresso é mostrada, independentemente se a compilação for ou não concluída com sucesso.</para>

<para>
<screenshot>
<screeninfo
>Criar o modelo</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="build.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Criar o modelo</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

  <para
>Usando este resultado descritivo é muito mais fácil descobrir problemas com os dados de entrada.</para>
</sect1>

<sect1 id="test">
  <title
>Testar o modelo</title>
  <para
>Aqui você pode testar o modelo de fala.</para>
  <para
>O &kmyapplication; irá testar o modelo definido na seção <guilabel
>Arquivos de saída</guilabel
>, na seção <link linkend="input_output"
>entrada e saída</link
>.</para>

  <para
>Durante os testes, o reconhecimento da infraestrutura selecionada será executado para reconhecer os arquivos de entrada definidos pelas mensagens de teste. Os erros de palavras e frases resultantes serão contados, assim como a robustez global, analisando os índices de confiança do reconhecimento.</para>

  <para
>A tela principal de teste irá mostrar todos os testes iniciados e indicar quando estiverem concluídos. Os testes em si serão executados ao mesmo tempo. A quantidade de paralelismo mais adequada a sua máquina é determinada automaticamente.</para>

<para>
<screenshot>
<screeninfo
>Testar o modelo</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="test_model.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Testar o modelo</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

<para
>Assim que todos os testes forem concluídos, os <link linkend="test_results"
>resultados do teste</link
> serão apresentados automaticamente.</para>


</sect1>

<sect1 id="test_results">
  <title
>Resultados do teste</title>

  <para
>Depois de <link linkend="test"
>testar o modelo</link
> com sucesso, você pode obter aqui um relatório detalhado da precisão do reconhecimento.</para>

  <sect2 id="scoring">
    <title
>Pontuação</title>

    <para
>Para refletir melhor o desempenho do reconhecimento, o &kmyapplication; usa vários resultados com classificações para os testes.</para>

    <para
>Uma palavra ou frase reconhecida corretamente será classificada com o índice de confiança obtido pela palavra. Se a palavra for reconhecida corretamente, mas outra errada tiver uma melhor classificação, a precisão deste reconhecimento será 0%.</para>

    <para
>A taxa de reconhecimento global é a média de todas as taxas de confiança. A taxa de correção global apresentada mostra a média de todas as pontuações de correção que representam a semelhança com que uma palavra de uma determinada frase foi reconhecida corretamente.</para>

  </sect2>

  <sect2 id="test_results_words">
    <title
>Palavra</title>

    <para
>O &kmyapplication; irá enumerar a precisão do reconhecimento para cada palavra individual.</para>

    <para>
    <screenshot>
    <screeninfo
>Resultados das palavras</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_words.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Resultados das palavras</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>

    <para
>Se você tiver amostras que contenham mais que uma palavra, estas serão segmentadas durante o reconhecimento. Cada palavra será classificada individualmente (ainda que as diferentes palavras se influenciam mutuamente).</para>
  </sect2>

  <sect2 id="test_results_sentences">
    <title
>Frases</title>
    <para
>Esta seção apresenta cada mensagem como uma "frase".</para>

    <para
>As mensagens que foram gravadas mais que uma vez serão combinadas.</para>

    <para>
    <screenshot>
    <screeninfo
>Resultados das frases</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_sentences.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Resultados das frases</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>
  </sect2>

  <sect2 id="test_results_files">
    <title
>Arquivos</title>
    <para
>Na seção dos arquivos, você pode ver os resultados do reconhecimento de cada arquivo. Cada um deles irá apresentar os <link linkend="scoring"
>10 resultados mais prováveis</link
> no painel de detalhes quando os selecionar.</para>

    <para>
    <screenshot>
    <screeninfo
>Resultados do arquivo</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_files.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Resultados do arquivo</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>

    <para
>Quando identificar amostras com problemas, você pode voltar a gravá-las (ou removê-las), selecionando-as e clicando em <guibutton
>Editar a amostra</guibutton
>.</para>

    <para
>Você pode ordenar os arquivos por cada coluna, bastando para isto clicar no cabeçalho da coluna. Desta forma, é muito fácil descobrir amostras inválidas, ordenando pela taxa de reconhecimento.</para>
  </sect2>

  <sect2 id="test_results_log">
    <title
>Registro</title>
    <para
>Aqui você poderá rever o protocolo completo deste subteste.</para>

    <para>
    <screenshot>
    <screeninfo
>Registro do teste</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_log.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Registro do teste</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>
  </sect2>
</sect1>



<sect1 id="reports">
  <title
>Relatórios</title>

  <para
>O &kmyapplication; pode exportar relatórios sobre os resultados dos testes.</para>

  <para
>A exportação de um relatório pode ser iniciada ao selecionar o item <guibutton
>Exportar o resultado do teste</guibutton
> na janela principal do &kmyapplication;.</para>

  <sect2 id="templates">
    <title
>Modelos</title>
    <para
>Os relatórios são criados ao preencher modelos com informações. Normalmente, estes modelos são arquivos de texto simples ou em LaTeX.</para>

    <para
>Três modelos em LaTeX, sendo dois em alemão e um em inglês, são fornecidos com o &kmyapplication;, mas é muito fácil adicionar novos. Basta instalar os novos modelos em um dos seguintes locais, dependendo do seu sistema operacional: <itemizedlist>
      <listitem
><para
>Linux: <filename
>~/.kde/share/apps/sam/reports/templates</filename
></para
></listitem>
      <listitem
><para
>Windows: <filename
>%appdata%\.kde\share\apps\sam\reports\templates</filename
></para
></listitem>
    </itemizedlist>
    </para>

    <para
>Os modelos existentes podem ser usados como referência. Eles podem ser encontrados no seguinte local: <itemizedlist>
      <listitem
><para
>Linux: <filename
>/usr/share/apps/sam/reports/templates</filename
></para
></listitem>
      <listitem
><para
>Windows: <filename
>&lt;installation path&gt;\share\apps\sam\reports\templates</filename
></para
></listitem>
    </itemizedlist>
    </para>
  </sect2>


  <sect2 id="parameters">
    <title
>Fornecendo metadados</title>

    <para
>O mecanismo de relatórios permite a introdução de metadados sobre os dados de entrada usados, assim como as condições dos testes, os locutores, etc.</para>

    <para>
    <screenshot>
    <screeninfo
>Relatório: Informações gerais</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_export_main.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Relatório: Informações gerais</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>

    <para
>Esta informação pode ser introduzida diretamente através da interface gráfica. Dependendo do modelo usado, nem todas as informações fornecidas poderão ser usadas pelo relatório exportado.</para>

    <para>
    <screenshot>
    <screeninfo
>Relatório: Informações da experiência</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_export_experiment.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Relatório: Informações da experiência</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>

    <para
>As informações introduzidas na janela serão também salvas com o seu arquivos de projeto do &kmyapplication;.</para>


    <para>
    <screenshot>
    <screeninfo
>Relatório: Conjunto de testes</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_export_testset.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Relatório: Informações do conjunto de testes</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>

    <para
>Alguns metadados, como a quantidade de amostras no seu conteúdo de testes e de treinamento, assim como as informações do sistema, poderão ser preenchidos de forma automática pelo &kmyapplication; ou manualmente.</para>

  </sect2>

  <sect2 id="output">
    <title
>Resultado</title>

    <para
>Os relatórios exportados são armazenados como arquivos ZIP. No caso dos modelos em LaTeX, eles poderão conter imagens para complementar o arquivo <filename
>.tex</filename
>.</para>

    <para
>Na imagem abaixo, você poderá ver o início de um relatório em LaTeX completo exportado, contendo um teste simples do cenário do Amarok com o modelo de base estático do Voxforge.</para>

    <para>
    <screenshot>
    <screeninfo
>Relatório exportado</screeninfo>
      <mediaobject>
        <imageobject>
          <imagedata fileref="test_result_export_pdf.png" format="PNG"/>
        </imageobject>
        <textobject>
          <phrase
>Relatório exportado</phrase>
        </textobject>
      </mediaobject>
    </screenshot>
    </para>
  </sect2>
</sect1>




<sect1 id="command_line_args">
  <title
>Argumentos da linha de comando</title>

  <para
>O &kmyapplication; oferece argumentos da linha de comando para as ações mais comuns que automatizam a criação e teste do modelo.</para>

  <para
>Todas as ações fornecidas como argumentos da linha de comando serão retiradas imediatamente após carregar o &kmyapplication;. As opções que dependem uma da outra (como a compilação e o teste) serão reordenadas automaticamente.</para>

<table frame='all'
><title
>Argumentos da linha de comando</title>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1'/>
<colspec colname='c2'/>
<thead>
<row>
  <entry
>Argumento</entry>
  <entry
>Descrição</entry>
</row>
</thead>
<tbody>
<row>
  <entry
><parameter
>-m &lt;tipo&gt;</parameter
></entry>
  <entry
>Seleciona a infraestrutura usada. Indique "sphinx" ou "htk" para selecionar a infraestrutura escolhida. Dependendo da sua instalação, o "sphinx" pode não estar disponível.</entry>
</row>
<row>
  <entry
><parameter
>-s</parameter
></entry>
  <entry
>Obtém todas as localizações do Simon; (equivalente a <guibutton
>Modificar o modelo do Simon</guibutton
>)</entry>
</row>
<row>
  <entry
><parameter
>-c</parameter
></entry>
  <entry
>Inicia a compilação do modelo;</entry>
</row>
<row>
  <entry
><parameter
>-t</parameter
></entry>
  <entry
>Inicia o teste do modelo;</entry>
</row>
<row>
  <entry
><parameter
>-e &lt;nome_arquivo&gt;</parameter
></entry>
  <entry
>Exporta um relatório para o nome do arquivo indicado;</entry>
</row>
<row>
  <entry
><parameter
>-b</parameter
></entry>
  <entry
>Ativa o processamento em lote. O &kmyapplication; será finalizado automaticamente após o processamento de todos os argumentos. A janela principal não será mostrada.</entry>
</row>
<row>
  <entry
><parameter
>-l &lt;nome_arquivo&gt;</parameter
></entry>
  <entry
>Exporta o registro de compilação completo para o arquivo indicado;</entry>
</row>
<row>
  <entry
><parameter
>-w</parameter
></entry>
  <entry
>Grava automaticamente todos os arquivos de projeto do &kmyapplication; ao sair do aplicativo;</entry>
</row>
<row>
  <entry
><parameter
>&lt;nome_arquivo&gt;</parameter
></entry>
  <entry
>Carrega este arquivo de projeto do &kmyapplication; ao iniciar.</entry>
</row>
</tbody>
</tgroup>
</table>

</sect1>

</chapter>


<chapter id="faq">
<title
>Perguntas e respostas</title>

<para
>Em um esforço para manter esta seção sempre atualizada, ela está disponível na nossa <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Troubleshooting_Guide"
>Wiki on-line</ulink
>. </para>

</chapter>

<chapter id="credits">
<title
>Créditos e licença</title>

<para
>&kmyapplication; </para>
<para
>Direitos autorais do programa 2008-2010 de Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

<para
>Direitos autorais da documentação &copy; 2009-2010 de Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

<para
>Tradução de André Marcelo Alvarenga <email
>alvarenga@kde.org</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Instalação</title>
<para
>Por favor, consulte as instruções de instalação na nossa <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Installation"
>wiki</ulink
>.</para>
</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
