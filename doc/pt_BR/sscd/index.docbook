<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kmyapplication "<application
>SSCd</application
>">
  <!ENTITY kappname "&kmyapplication;">
  <!ENTITY package "kde-module">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE">
]>

<book id="SSCd" lang="&language;">

<bookinfo>

<othercredit role="translator"
><firstname
>André Marcelo</firstname
><surname
>Alvarenga</surname
><affiliation
><address
><email
>alvarenga@kde.org</email
></address
></affiliation
><contrib
>Tradução</contrib
></othercredit
> 

<date
>13/12/2012</date>
<releaseinfo
>0.4</releaseinfo>

<abstract>
<para
>O &kmyapplication; é o componente de servidor da ferramenta de aquisição de amostras SSC. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>Kapp</keyword>
<keyword
>Simon</keyword>
<keyword
>amostra</keyword>
<keyword
>fala</keyword>
<keyword
>voz</keyword>
<keyword
>aquisição</keyword>
<keyword
>gravação</keyword>
<keyword
>acessibilidade</keyword>
</keywordset>

<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2009-2012</year>
<holder
>Peter Grasch</holder>
</copyright>

<authorgroup>
<author
><personname
> <firstname
>Peter</firstname
> <othername
>H.</othername
> <surname
>Grasch</surname
> </personname
> <email
>peter.grasch@bedahr.org</email
> </author>
</authorgroup>


<title
>Manual do &kmyapplication;</title>
</bookinfo>

<chapter id="introduction">
<title
>Introdução</title>

<para
>O &kmyapplication; é o componente de servidor da ferramenta de aquisição de amostras SSC. </para>
<para
>Ele gerencia os dados do locutor (usuários, instituições), assim como os as amostras e os seus metadados, com o uso de um banco de dados. </para>
<para
>Recebe os dados dos clientes SSC que se conectam ao servidor através usando TCP/IP. </para>

<para
>Para mais informações sobre a arquitetura geral do pacote Simon, veja o <ulink url="help:/simon/overview.html#architecture"
>Manual do Simon</ulink
>. Para mais informações sobre o cliente SSC, veja o <ulink url="help:/ssc"
>Manual do SSC</ulink
>.</para>
</chapter>

<chapter id="using-kapp">
<title
>Usando o &kmyapplication;</title>

<para
>O &kmyapplication; é um aplicativo de linha de comando que não tem qualquer interface para o usuário. Não existem parâmetros de lançamento especiais.</para>


<sect1 id="sscd_folder">
  <title
>Pasta base</title>
  <para
>A pasta base do &kmyapplication; contém o arquivo de configuração <filename
>sscd.conf</filename
> e um registro de erros, no caso de acontecer algo de errado. Esta pasta também contém a subpasta <filename
>samples</filename
>, onde estão armazenadas todas as amostras.</para>

<para
>A localização da pasta do SSCd depende do seu sistema operacional: <table frame='all'
><title
>Pasta base do SSC</title>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1'/>
<colspec colname='c2'/>
<thead>
<row>
  <entry
>Microsoft Windows</entry>
  <entry
>GNU/Linux</entry>
</row>
</thead>
<tbody>
<row>
  <entry
><filename
>A pasta de instalação do sscd.exe (normalmente: C:\ProgramFiles\simon 0.3\bin\sscd.exe</filename
>)</entry>
  <entry
><filename
>/usr/share/sscd</filename
></entry>
</row>
</tbody>
</tgroup>
</table>
</para>
</sect1>

<sect1 id="configuration">
  <title
>Configuração</title>

  <para
>Não existe nenhuma configuração gráfica do SSCd, mas existe um arquivo de configuração (o <filename
>sscd.conf</filename
>), armazenado na <link linkend="sscd_folder"
>pasta do SSCd</link
>.</para>

  <para
>O arquivo de configuração padrão está bem documentado e deverá ser fácil de entender.</para>

  <para
>Antes de executar o SSCd, você poderá querer alterar pelo menos os itens 'DatabaseUser' e 'DatabasePassword' do arquivo de configuração. Veja a <link linkend="database"
>seção do banco de dados</link
> para obter mais informações.</para>

  <screen
>; Este é um arquivo de configuração de exemplo e mostra as definições
; predefinidas; o sscd irá procurar por este arquivo em:
; Linux:
;    /usr/share/sscd/sscd.conf
; Windows:
;    &lt;pasta de instalação do SSCd&gt;\sscd.conf

[General]
; Mude isto para usar um banco de dados diferente; Porque o SSCd usa
; comandos específicos do banco de dados em alguns locais, no momento ; só é suportado o QMYSQL.
; O suporte a outros SGBDs poderá ser adicionado com extrema facilidade,
; e poderá ser solicitado suporte através do endereço
; kde-accessibility@kde.org
DatabaseType=QMYSQL

; O servidor do SGBD
DatabaseHost=127.0.0.1

; A porta do SGBD; 3306 é a porta padrão do MySQL
DatabasePort=3306

; O banco de dados a ser usado; certifique-se de executar o script de
; criação indicado antes de usar o SSCd
DatabaseName=ssc

; O nome de usuário usado para se conectar ao SGBD
DatabaseUser=usuariossc

; A senha do banco de dados. O valor padrão não irá funcionar na maioria
; dos casos
DatabasePassword=ALTERE-ME AGORA

; Opções do banco de dados. Veja detalhes na documentação do
; QSqlDatabase do Qt
DatabaseOptions=MYSQL_OPT_RECONNECT=1

; A porta onde o servidor irá atender as solicitações; Padrão: 4440
Port=4440

; Associa o servidor a um cliente IP específico; Se este valor for
; verdadeiro, o servidor irá ignorar as solicitações de todos os
; endereços menos o BoundHost (veja abaixo)
Bind=false

; O IP do servidor associado (se Bind estiver ativo "true")
BindHost=127.0.0.1
  </screen>
</sect1>

<sect1 id="database">
  <title
>Banco de dados</title>

  <para
>O &kmyapplication; armazena os dados do locutor e das amostras (mas não as amostras em si) em um banco de dados. No momento, só são suportados os bancos de dados MySQL. No entanto, a adição de suporte para um novo banco de dados é simples. Contacte a <ulink url="mailto:kde-accessibility@kde.org"
>equipe do Simon</ulink
> se precisar de ajuda.</para>

  <para
>Para configurar as tabelas necessárias, o &kmyapplication; fornece um script de criação apropriado <filename
>mysql_create_script.sql</filename
> que é instalado na <link linkend="sscd_folder"
>pasta base</link
> do &kmyapplication;.</para>


  <para
>Os erros do banco de dados podem ser encontrados no arquivo <filename
>error.log</filename
>, que também está localizado na pasta base.</para>

</sect1>

<sect1 id="locked_mode">
  <title
>Modo bloqueado</title>


  <para
>O SSCd recebe opcionalmente o argumento da linha de comando "-l" (ou "--locked") para ativar o modo "bloqueado" (desligado por padrão).</para>

  <para
>No modo bloqueado, os clientes podem ainda conectar-se e enviar amostras, assim como criar novos microfones e placas de som, mas não terão qualquer acesso (de leitura ou escrita) para quaisquer dados pessoais ou institucionais, além do ID e do nome do usuário. Isto pode ser útil para limitar a quantidade de informações privadas dos pacientes que são apresentadas às equipes de gravação.</para>

  <para
>Enquanto o modo bloqueado estiver ativo, não existe também nenhuma pesquisa dos usuários, por isso certifique-se que a sua equipe de gravação tem uma lista dos usuários de antemão.</para>
</sect1>

<sect1 id="extracting_samples">
  <title
>Extração de amostras coletadas</title>

  <para
>Para criar os modelos com as amostras coletadas pelo &kmyapplication;, você tem que primeiro extraí-las do banco de dados.</para>

  <warning>
    <para
>Como o &kmyapplication; é desenvolvido para aquisição de amostras em grande escala, não está preparado para ser amigável. A documentação abaixo é fornecida em primeiro lugar para profissionais com grande competência técnica. </para>
    <para
>A maioria dos scripts abaixo necessitam das ferramentas da GNU (normalmente disponíveis por padrão no GNU/Linux). </para>
  </warning>

  <para
>Você pode usar a seguinte pesquisa (pode necessitar de alguns ajustes, dependendo de quais amostras você precisa): <screen
>use ssc;

select s.Path, s.Prompt
  from Sample s inner join User u
    on s.UserId = u.UserId inner join UserInInstitution uii
      on u.UserId = uii.UserId inner join SampleType st
        on s.TypeId = st.SampleTypeId inner join Microphone m
          on m.MicrophoneId = s.MicrophoneId
  WHERE st.ExactlyRepeated=1 and uii.InstitutionId = 3
    and (m.MicrophoneId = 1);
    </screen>
  </para>

  <para
>Esta pesquisa irá listar todas as amostras da instituição 3 que foram gravadas com o microfone 1.</para>

  <para
>Você pode, por exemplo, usar este script para criar um arquivo de mensagens: <screen
>#!/bin/bash
      sed '1d' $1 
> temp_out
      sed -e 's/\\\\/\//g' -e 's/.*Samples\///g' -e 's/\.wav\t/ /' temp_out 
> $1
      rm temp_out
    </screen>
  </para>

  <para
>Este arquivo de mensagens poderá então ser <ulink url="help:/simon/training.html#import_training-data"
>importado no Simon</ulink
>. </para>

  <para
>Para criar o dicionário apropriado para compilar o modelo, você pode querer listar todas as frases que existem no arquivo de mensagens. Você pode fazer isto com este script: <screen
>#!/bin/bash
      cat $1 | sed -e 's/[0-9\/]* //' | sort | uniq
    </screen>
  </para>
</sect1>
</chapter>


<chapter id="faq">
<title
>Perguntas e respostas</title>

<para
>Em um esforço para manter esta seção sempre atualizada, ela está disponível na nossa <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Troubleshooting_Guide"
>Wiki on-line</ulink
>. </para>

</chapter>

<chapter id="credits">
<title
>Créditos e licença</title>

<para
>&kmyapplication; </para>
<para
>Direitos autorais do programa 2008-2010 de Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

<para
>Direitos autorais da documentação &copy; 2009-2010 de Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

<para
>Tradução de André Marcelo Alvarenga <email
>alvarenga@kde.org</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Instalação</title>
<para
>Por favor, consulte as instruções de instalação na nossa <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Installation"
>wiki</ulink
></para>
</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
