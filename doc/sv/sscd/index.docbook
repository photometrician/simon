<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kmyapplication "<application
>SSCd</application
>">
  <!ENTITY kappname "&kmyapplication;">
  <!ENTITY package "kde-module">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE">
]>

<book id="SSCd" lang="&language;">

<bookinfo>

<othercredit role="translator"
> <firstname
>Stefan</firstname
> <surname
>Asserhäll</surname
> <affiliation
><address
><email
>stefan.asserhall@comhem.se</email
></address
></affiliation
> <contrib
>Översättare</contrib
></othercredit
> 

<date
>2012-12-13</date>
<releaseinfo
>0.4</releaseinfo>

<abstract>
<para
>&kmyapplication; är serverkomponenten i verktyget för insamling av samplingar, SSC. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>Kapp</keyword>
<keyword
>Simon</keyword>
<keyword
>sampling</keyword>
<keyword
>tal</keyword>
<keyword
>röst</keyword>
<keyword
>insamling</keyword>
<keyword
>inspelning</keyword>
<keyword
>handikappstöd</keyword>
</keywordset>

<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2009-2012</year>
<holder
>Peter Grasch</holder>
</copyright>

<authorgroup>
<author
><personname
> <firstname
>Peter</firstname
> <othername
>H.</othername
> <surname
>Grasch</surname
> </personname
> <email
>peter.grasch@bedahr.org</email
> </author>
</authorgroup>


<title
>Handbok &kmyapplication;</title>
</bookinfo>

<chapter id="introduction">
<title
>Inledning</title>

<para
>&kmyapplication; är serverkomponenten i verktyget för insamling av samplingar, SSC. </para>
<para
>Programmet hanterar information om inläsare (användarnamn, institutioner) samt metadata för samplingar med användning av en databas, och lagrar samplingarna. </para>
<para
>Det tar emot indata från SSC-klienter, som ansluter till servern via TCP/IP. </para>

<para
>För mer information om den allmänna arkitekturen för Simon-programvaran, se <ulink url="help:/simon/overview.html#architecture"
>Handbok Simon</ulink
>. För information om SSC-klienten, se <ulink url="help:/ssc"
>Handbok SSC</ulink
>.</para>
</chapter>

<chapter id="using-kapp">
<title
>Använda &kmyapplication;</title>

<para
>Programmet &kmyapplication; är ett kommandoradsprogram, som inte har något användargränssnitt. Det finns inga speciella väljare vid start.</para>


<sect1 id="sscd_folder">
  <title
>Baskatalog</title>
  <para
>Baskatalogen för &kmyapplication; innehåller inställningsfilen <filename
>sscd.conf</filename
> och en fellogg om något går fel. Katalogen innehåller också underkatalogen <filename
>samples</filename
> där alla samplingar lagras.</para>

<para
>Platsen för katalogen SSCd beror på operativsystemet: <table frame='all'
><title
>Baskatalog för SSC</title>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1'/>
<colspec colname='c2'/>
<thead>
<row>
  <entry
>Microsoft Windows</entry>
  <entry
>GNU/Linux</entry>
</row>
</thead>
<tbody>
<row>
  <entry
><filename
>Installationskatalogen för sscd.exe (oftast C:\Program Files\simon 0.3\bin\sscd.exe</filename
>)</entry>
  <entry
><filename
>/usr/share/sscd</filename
></entry>
</row>
</tbody>
</tgroup>
</table>
</para>
</sect1>

<sect1 id="configuration">
  <title
>Inställning</title>

  <para
>Det finns ingen grafisk inställning av SSCd, men det finns en inställningsfil (<filename
>sscd.conf</filename
>) som lagras i <link linkend="sscd_folder"
>katalogen för SSCd</link
>.</para>

  <para
>Standardversionen av inställningsfilen har en mängd kommentarer och bör vara självförklarlig.</para>

  <para
>Innan SSCd körs, bör du åtminstone ändra posterna DatabaseUser och DatabasePassword i inställningsfilen. Se <link linkend="database"
>avsnittet om databasen</link
> för mer information.</para>

  <screen
>; This is an example config file and displays the built-in defaults
; SSCd will look for this file in:
; Linux:
;    /usr/share/sscd/sscd.conf
; Windows:
;    &lt;sscd installation path&gt;\sscd.conf

[General]
; Change this to use a different database; Because SSCd uses db-specific
; commands in places, only QMYSQL is supported at the moment.
; Support for other DBMS can be added extremely easily, though so please
; feel free to request support through kde-accessibility@kde.org
DatabaseType=QMYSQL

; The host of the DBMS
DatabaseHost=127.0.0.1

; The port of the DBMS; 3306 is the default port of MySQL
DatabasePort=3306

; The database to use; Make sure that you run the supplied create script
; before you use SSCd
DatabaseName=ssc

; The username to use when connecting to the DBMS
DatabaseUser=sscuser

; Database password. The default one will obviously not work in most cases
DatabasePassword=CHANGE ME NOW

; Database options. Refer to Qts documentation of QSqlDatabase for details
DatabaseOptions=MYSQL_OPT_RECONNECT=1

; The port the server will listen to; Default: 4440
Port=4440

; Bind the server to a specific client IP; If this is true, the server
; will ignore requests from all but the BoundHost (see below)
Bind=false

; IP of the bound host (if Bind is active)
BindHost=127.0.0.1
  </screen>
</sect1>

<sect1 id="database">
  <title
>Databas</title>

  <para
>&kmyapplication; lagrar inläsare och information om samplingar (men inte själva samplingarna) i en databas. För närvarande stöds bara MySQL-databaser fullständigt. Att lägga till stöd för en ny databas är dock mycket enkelt. Kontakta <ulink url="mailto:kde-accessibility@kde.org"
>Simon-gruppen</ulink
> om du vill hjälpa till.</para>

  <para
>För att skapa nödvändiga tabeller, levereras &kmyapplication; med ett lämpligt skript <filename
>mysql_create_script.sql</filename
> installerat i <link linkend="sscd_folder"
>baskatalogen</link
> för &kmyapplication;.</para>


  <para
>Databasfel kan hittas i <filename
>error.log</filename
> som också finns i baskatalogen.</para>

</sect1>

<sect1 id="locked_mode">
  <title
>Låst läge</title>


  <para
>SSCd kan valfritt använda kommandoradsväljaren  "-l" (eller "--locked") för att aktivera "låst" läge (normalt inaktivt).</para>

  <para
>I låst läge kan klienter fortfarande ansluta och ladda upp samplingar samt skapa nya mikrofoner och ljudkort, men kan inte komma åt (läsa eller skriva) någon personlig eller institutionsdata förutom användarnas namn och id. Det kan vara till hjälp för att begränsa den mängd privat patientinformation som visas för inspelningsteam.</para>

  <para
>När låst läge är aktivt finns det inte heller någon sökning av användare, så försäkra dig om att en lista med användar-id tillhandahålls till inspelningsteamet i förväg.</para>
</sect1>

<sect1 id="extracting_samples">
  <title
>Extrahera insamlade samplingar</title>

  <para
>För att bygga modeller med samplingarna som samlats in med &kmyapplication; måste du först extrahera dem från databasen.</para>

  <warning>
    <para
>Eftersom &kmyapplication; är konstruerat för insamling av samplingar i stor skala, är det inte användarvänligt. Dokumentationen nedan tillhandahålls i huvudsak för tekniskt skickliga proffs. </para>
    <para
>De flesta skript nedan kräver GNU-verktygen (oftast standardmässigt tillgängliga på GNU/Linux). </para>
  </warning>

  <para
>Du kan använda följande förfrågan (mindre justeringar är nödvändiga beroende på exakt vilka samplingar du behöver): <screen
>use ssc;

select s.Path, s.Prompt
  from Sample s inner join User u
    on s.UserId = u.UserId inner join UserInInstitution uii
      on u.UserId = uii.UserId inner join SampleType st
        on s.TypeId = st.SampleTypeId inner join Microphone m
          on m.MicrophoneId = s.MicrophoneId
  WHERE st.ExactlyRepeated=1 and uii.InstitutionId = 3
    and (m.MicrophoneId = 1);
    </screen>
  </para>

  <para
>Denna förfrågan listar samplingar från institution 3 som spelades in med mikrofon 1.</para>

  <para
>Därefter kan du exempelvis använda detta skript för att skapa en frågefil: <screen
>#!/bin/bash
      sed '1d' $1 
> temp_out
      sed -e 's/\\\\/\//g' -e 's/.*Samples\///g' -e 's/\.wav\t/ /' temp_out 
> $1
      rm temp_out
    </screen>
  </para>

  <para
>Denna frågefil kan därefter <ulink url="help:/simon/training.html#import_training-data"
>importeras i SImon</ulink
>. </para>

  <para
>För att bygga den lämpliga ordlistan för att kompilera modellen, kan du också behöva lista alla meningar som finns i frågefilen. Du kan göra det med detta skript: <screen
>#!/bin/bash
      cat $1 | sed -e 's/[0-9\/]* //' | sort | uniq
    </screen>
  </para>
</sect1>
</chapter>


<chapter id="faq">
<title
>Frågor och svar</title>

<para
>För att alltid försöka hålla avsnittet uppdaterat är det tillgängligt på vår <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Troubleshooting_Guide"
>wiki</ulink
>. </para>

</chapter>

<chapter id="credits">
<title
>Tack till och licens</title>

<para
>&kmyapplication; </para>
<para
>Program copyright 2008-2010 Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

<para
>Dokumentation Copyright &copy; 2009-2010 Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

<para
>Översättning Stefan Asserhäll <email
>stefan.asserhall@comhem.se</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Installation</title>
<para
>Se vår <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Installation"
>wiki</ulink
> för installationsinstruktioner.</para>
</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
