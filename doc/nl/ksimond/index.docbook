<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kmyapplication "<application
>KSimond</application
>">
  <!ENTITY kappname "&kmyapplication;">
  <!ENTITY package "kde-module">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Dutch "INCLUDE">
]>

<book id="ksimond" lang="&language;">


<bookinfo>

&Freek.de.Kruijf; 

<date
>2012-12-13</date>
<releaseinfo
>0.4</releaseinfo>

<abstract>
<para
>&kmyapplication; is een front-end voor de component Simond-server van de spraakherkenningssuite Simon. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>Kapp</keyword>
<keyword
>Simon</keyword>
<keyword
>herkenning</keyword>
<keyword
>spraakinvoer</keyword>
<keyword
>stem</keyword>
<keyword
>commando</keyword>
<keyword
>besturing</keyword>
<keyword
>toegankelijkheid</keyword>
</keywordset>

<!-- Translators: put here the copyright notice of the translation -->
<!-- Put here the FDL notice.  Read the explanation in fdl-notice.docbook
     and in the FDL itself on how to use it. -->
<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2009-2012</year>
<holder
>Peter Grasch</holder>
</copyright>

<authorgroup>
<author
><personname
> <firstname
>Peter</firstname
> <othername
>H.</othername
> <surname
>Grasch</surname
> </personname
> <email
>peter.grasch@bedahr.org</email
> </author>
</authorgroup>


<title
>Het handboek van &kmyapplication;</title>
</bookinfo>

<chapter id="introduction">
<title
>Inleiding</title>

<para
>&kappname; is een eenvoudige grafische front-end van Simond. Het maakt en toont een pictogram in de systeembalk waarmee u de Simond-server kun stoppen en starten. Verder biedt het een instellingendialoog waarin het de instellingen van Simond inbed. </para>
<para
>KSimond kan ingesteld worden om te worden gestart bij het starten van KDE. </para>
<para
>Rapporteer alle problemen of verzoeken om functies op <ulink url="http://sourceforge.net/tracker/?group_id=190872"
>onze bug-tracker</ulink
>. </para>
</chapter>

<chapter id="using-kapp">
<title
>&kappname; gebruiken</title>

<para
>Vanwege de minimalistische natuur van KSimond zelf, heeft deze zelfs geen toepassingsvenster. Het pictogram in het systeemvak echter heeft een contextmenu waarmee u Simond kunt starten en stoppen en ook de instellingen kunt openen. <screenshot>
<screeninfo
>schermafdruk van &kappname;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="ksimond.png" format="PNG"/>
	  </imageobject>
	  <textobject>
           <phrase
>"schermafdruk" van KSimond</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>

<para
>KSimond heeft de mogelijkheid om automatisch op te starten wanneer KDE opstart en ook onverwachte crashes van Simond af te handelen. <screenshot>
<screeninfo
>schermafdruk van de instellingen van &kappname;</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="ksimond_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Instellingen van KSimond</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>
<para
>merk op dat de optie om KSimond op te starten bij aanmelden zowel werkt onder Microsoft Windows als wanneer u KDE onder Linux gebruikt. Ondersteuning voor andere bureaubladomgevingen zoals Gnome, XFCE, etc. kan vereisen KSimond handmatig in het automatisch starten van de sessie op te nemen (kijk in de respectievelijke handleidingen van uw bureaubladomgeving). </para>

<para
>Naast de instellingen voor KSimond is de de lijst van instellingsmodules van Simond beschikbaar (ook beschikbaar via systeeminstellingen). Zie het <ulink url="help:/simond"
>Handboek van Simond</ulink
> voor details. <screenshot>
<screeninfo
>schermafdruk van de gebruikersinstellingen van Simond</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="simond_user_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Gebruikersinstellingen van Simond</phrase>
    </textobject>
  </mediaobject>
</screenshot>

<screenshot>
<screeninfo
>schermafdruk van de netwerkinstellingen van Simond</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="simond_network_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Netwerkinstellingen van Simond</phrase>
    </textobject>
  </mediaobject>
</screenshot>

<screenshot>
<screeninfo
>schermafdruk van de instellingen van het spraakmodel</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="simond_speech_model_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>Instellingen van het spraakmodel van Simond</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

</chapter>


<chapter id="credits">

<title
>Dankbetuigingen en licentie</title>

<para
>&kappname; </para>
<para
>Programma copyright 2008-2010 Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>
<!--<para>
Contributors:
<itemizedlist>
<listitem
><para
>Konqui the KDE Dragon <email
>konqui@kde.org</email
></para>
</listitem>
<listitem
><para
>Tux the Linux Penguin <email
>tux@linux.org</email
></para>
</listitem>
</itemizedlist>
</para
>-->

<para
>Documentatie Copyright &copy; 2010 Peter Grasch <email
>peter.grasch@bedahr.org</email
> </para>

&meld.fouten;&vertaling.freek; 
&underFDL; &underGPL; </chapter>


<appendix id="installation">
<title
>Installatie</title>
<para
>Kijk op onze <ulink url="http://userbase.kde.org/Special:myLanguage/Simon/Installation"
>wiki</ulink
> voor instructies over installatie.</para>
</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
