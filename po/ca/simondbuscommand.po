# Translation of simondbuscommand.po to Catalan
# Copyright (C) 2012 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@orange.es>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: simondbuscommand\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2012-11-05 01:49+0100\n"
"PO-Revision-Date: 2012-11-10 11:40+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@orange.es>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#. i18n: ectx: property (text), widget (QLabel, label)
#: createdbuscommandwidget.ui:20
msgid "Service name:"
msgstr "Nom de servei:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: createdbuscommandwidget.ui:33
msgid "Path:"
msgstr "Camí:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: createdbuscommandwidget.ui:46
msgid "Method:"
msgstr "Mètode:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: createdbuscommandwidget.ui:56
msgid "Arguments:"
msgstr "Arguments:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: createdbuscommandwidget.ui:69
msgid "Interface:"
msgstr "Interfície:"

#: dbuscommand.cpp:35 dbuscommandmanager.cpp:48
msgid "D-Bus"
msgstr "D-Bus"

#: dbuscommand.cpp:60
msgctxt "Name of the service"
msgid "Service Name"
msgstr "Nom de servei"

#: dbuscommand.cpp:61
msgctxt "Name of the path"
msgid "Path"
msgstr "Camí"

#: dbuscommand.cpp:62
msgctxt "Name of the D-Bus interface"
msgid "Interface"
msgstr "Interfície"

#: dbuscommand.cpp:63
msgctxt "Name of the method to call"
msgid "Method"
msgstr "Mètode"

#: dbuscommand.cpp:64
msgctxt "Arguments of the call"
msgid "Arguments"
msgstr "Arguments"