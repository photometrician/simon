# Copyright (C) 2012 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Translation of ksimond.po to Brazilian Portuguese
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2012.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: ksimond\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2012-12-22 02:02+0100\n"
"PO-Revision-Date: 2012-12-22 10:21-0200\n"
"Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "André Marcelo Alvarenga"

msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alvarenga@kde.org"

#. i18n: ectx: label, entry (AutoStart), group (KSimond)
#. i18n: ectx: tooltip, entry (AutoStart), group (KSimond)
#: config/ksimondconfiguration.kcfg:11 config/ksimondconfiguration.kcfg:13
msgid "Whether we want to start ksimond when KDE is started."
msgstr "Se deseja iniciar o KSimond ao iniciar o KDE."

#. i18n: ectx: label, entry (AutoStartSimond), group (KSimond)
#. i18n: ectx: tooltip, entry (AutoStartSimond), group (KSimond)
#: config/ksimondconfiguration.kcfg:16 config/ksimondconfiguration.kcfg:18
msgid "Whether we want to start simond automatically."
msgstr "Se deseja iniciar o Simond automaticamente."

#. i18n: ectx: label, entry (AutoReStartSimond), group (KSimond)
#. i18n: ectx: tooltip, entry (AutoReStartSimond), group (KSimond)
#: config/ksimondconfiguration.kcfg:21 config/ksimondconfiguration.kcfg:23
msgid ""
"Whether we want to start simond automatically after it has quit unexpectedly."
msgstr ""
"Se deseja iniciar o Simond automaticamente se ele for fechado de forma "
"inesperada."

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoStart)
#: config/ksimondconfiguration.ui:20
msgid "Start KSimond when logging in"
msgstr "Iniciar o KSimond ao iniciar a sessão"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoStartSimond)
#: config/ksimondconfiguration.ui:27
msgid "Start Simond when KSimond starts"
msgstr "Iniciar o Simond ao iniciar o KSimond"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoReStartSimond)
#: config/ksimondconfiguration.ui:34
msgid "Restart Simond if it quits unexpectedly"
msgstr "Reiniciar o Simond se ele for fechado de forma inesperada"

#: src/ksimondview.cpp:55
msgid "Simond"
msgstr "Simond"

#: src/ksimondview.cpp:58 src/ksimondview.cpp:86
msgid "Start Simond"
msgstr "Iniciar o Simond"

#: src/ksimondview.cpp:63 src/ksimondview.cpp:85
msgid "Start Simon"
msgstr "Iniciar o Simon"

#: src/ksimondview.cpp:68 src/ksimondview.cpp:87
msgid "Restart Simond"
msgstr "Reiniciar o Simond"

#: src/ksimondview.cpp:74 src/ksimondview.cpp:88
msgid "Stop Simond"
msgstr "Para o Simond"

#: src/ksimondview.cpp:80 src/ksimondview.cpp:89
msgid "Configuration"
msgstr "Configuração"

#: src/ksimondview.cpp:118
msgid "simond is already running"
msgstr "O simond já está em execução"

#: src/ksimondview.cpp:139
msgid "simond stopped"
msgstr "O simond foi interrompido"

#: src/ksimondview.cpp:154
msgid "simond started"
msgstr "O simond foi iniciado"

#: src/ksimondview.cpp:182
#, kde-format
msgid ""
"Could not start simond.\n"
"\n"
"Please check your configuration.\n"
"\n"
"Command: \"%1\""
msgstr ""
"Não foi possível iniciar o simond.\n"
"\n"
"Verifique a sua configuração.\n"
"\n"
"Comando: \"%1\""

#: src/ksimondview.cpp:186
#, kde-format
msgctxt "%1 is a status message"
msgid "simond crashed. (Status: %1)"
msgstr "O simond falhou. (Status: %1)"

#: src/ksimondview.cpp:190
msgid "Timeout."
msgstr "Fim do tempo limite."

#: src/ksimondview.cpp:193
msgid "Could not communicate with simond: Write failed."
msgstr ""
"Não foi possível estabelecer comunicação com o simond: A gravação falhou."

#: src/ksimondview.cpp:196
msgid "Could not communicate with simond: Read failed."
msgstr ""
"Não foi possível estabelecer comunicação com o simond: A leitura falhou."

#: src/ksimondview.cpp:199
msgid "Unknown Error"
msgstr "Erro desconhecido"

#: src/main.cpp:28
msgid "A KDE 4 frontend for Simond"
msgstr "Uma interface gráfica do Simond para o KDE 4"

#: src/main.cpp:32
msgid "KSimond"
msgstr "KSimond"

#: src/main.cpp:33
msgid "(C) 2008 Peter Grasch"
msgstr "(C) 2008 Peter Grasch"

#: src/main.cpp:34
msgid "Peter Grasch"
msgstr "Peter Grasch"