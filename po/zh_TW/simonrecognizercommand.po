# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2012-08-20 02:34+0200\n"
"PO-Revision-Date: 2013-02-01 12:49+0800\n"
"Last-Translator: Franklin Weng <franklin at goodhorse dot idv dot tw>\n"
"Language-Team: Chinese Traditional <kde-tw@googlegroups.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 1.5\n"

msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Frank Weng (a.k.a. Franklin)"

msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "franklin at goodhorse dot idv dot tw"

#: recognizercommandmanager.cpp:47 recognizerconfiguration.cpp:30
#: recognizerconfiguration.cpp:31
msgid "Recognizer"
msgstr "辨識器"

#. i18n: ectx: property (text), widget (QLabel, lbAppend)
#: recognizerconfigurationdlg.ui:25
msgid "Append text after result:"
msgstr "在結果後附加文字："