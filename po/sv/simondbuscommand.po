# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhall <stefan.asserhall@comhem.se>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2012-11-05 01:49+0100\n"
"PO-Revision-Date: 2012-11-19 17:13+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@comhem.se>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: property (text), widget (QLabel, label)
#: createdbuscommandwidget.ui:20
msgid "Service name:"
msgstr "Tjänstnamn:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: createdbuscommandwidget.ui:33
msgid "Path:"
msgstr "Sökväg:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: createdbuscommandwidget.ui:46
msgid "Method:"
msgstr "Metod:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: createdbuscommandwidget.ui:56
msgid "Arguments:"
msgstr "Argument:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: createdbuscommandwidget.ui:69
msgid "Interface:"
msgstr "Gränssnitt:"

#: dbuscommand.cpp:35 dbuscommandmanager.cpp:48
msgid "D-Bus"
msgstr "D-Bus"

#: dbuscommand.cpp:60
msgctxt "Name of the service"
msgid "Service Name"
msgstr "Tjänstnamn"

#: dbuscommand.cpp:61
msgctxt "Name of the path"
msgid "Path"
msgstr "Sökväg"

#: dbuscommand.cpp:62
msgctxt "Name of the D-Bus interface"
msgid "Interface"
msgstr "Gränssnitt"

#: dbuscommand.cpp:63
msgctxt "Name of the method to call"
msgid "Method"
msgstr "Metod"

#: dbuscommand.cpp:64
msgctxt "Arguments of the call"
msgid "Arguments"
msgstr "Argument"