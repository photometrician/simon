# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhall <stefan.asserhall@comhem.se>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2013-06-05 02:01+0000\n"
"PO-Revision-Date: 2012-11-19 17:10+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@comhem.se>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"

msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll"

msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@comhem.se"

#: accuracydisplay.cpp:37
#, kde-format
msgctxt ""
"%1 is the amount of correctly recognized samples, %2 is the amount of total "
"samples"
msgid "Recognized %1 of %2: Recognition rate:"
msgstr "Kände igen %1 av %2: Igenkänningsförhållande:"

#: conservativetraining.cpp:36
msgid "Conservative Training"
msgstr "Konservativ övning"

#: conservativetraining.cpp:74
msgid "Could not open output prompts"
msgstr "Kunde inte öppna utdatafrågor"

#. i18n: ectx: property (text), widget (QLabel, label)
#: conservativetraining.ui:25
msgid "Input folder:"
msgstr "Inkatalog:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: conservativetraining.ui:39
msgid "Output prompts:"
msgstr "Utdatafrågor:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: conservativetraining.ui:53
msgid "Minimum confidence:"
msgstr "Minimal konfidens:"

#. i18n: ectx: property (toolTip), widget (QDoubleSpinBox, sbMinimumConfidence)
#: conservativetraining.ui:60
msgid ""
"You can limit the import to samples that were recognized with at least the "
"following confidence rate (range: 0 - 1; Setting this to 0 means to import "
"everything):"
msgstr ""
"Du kan begränsa importen till samplingar som kändes igen med minst följande "
"konfidensintervall (intervall: 0 - 1, att ställa in det till 0 betyder att "
"allt importeras):"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: conservativetraining.ui:81
msgid "Handle global filter"
msgstr "Hantera globalt filter"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: conservativetraining.ui:93
msgid "Activate filter:"
msgstr "Aktivera filter:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: conservativetraining.ui:103
msgid "Deactivate filter:"
msgstr "Inaktivera filter:"

#. i18n: ectx: property (text), widget (QLabel, label)
#. i18n: ectx: property (text), widget (QLabel, label_13)
#. i18n: ectx: property (text), widget (QLabel, label_28)
#. i18n: ectx: property (text), widget (QLabel, label_27)
#. i18n: ectx: property (text), widget (QLabel, label_7)
#. i18n: ectx: property (text), widget (QLabel, label_10)
#. i18n: ectx: property (text), widget (QLabel, label_19)
#: corpusinformation.ui:17 exporttestresultsdlg.ui:40
#: exporttestresultsdlg.ui:125 exporttestresultsdlg.ui:202
#: exporttestresultsdlg.ui:219 exporttestresultsdlg.ui:278
#: juliustestconfigurationwidget.ui:34 sphinxtestconfigurationwidget.ui:31
msgid "Tag:"
msgstr "Tagg:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#. i18n: ectx: property (text), widget (QLabel, label_9)
#. i18n: ectx: property (text), widget (QLabel, label_18)
#: corpusinformation.ui:27 exporttestresultsdlg.ui:229
#: exporttestresultsdlg.ui:288
msgid "Notes:"
msgstr "Anteckningar:"

#. i18n: (People)
#. i18n: ectx: property (text), widget (QLabel, label_26)
#: corpusinformation.ui:41
msgid "Speakers:"
msgstr "Talare:"

#. i18n: ectx: property (text), widget (QLabel, label_24)
#: corpusinformation.ui:58
msgid "Samples:"
msgstr "Samplingar:"

#. i18n: ectx: property (text), widget (KPushButton, pbAutomaticallyDetermine)
#: corpusinformation.ui:77
msgid "Determine automatically"
msgstr "Bestäm automatiskt"

#: exporttestresults.cpp:48
msgid "Export test results"
msgstr "Exportera provresultat"

#: exporttestresults.cpp:93
#, kde-format
msgctxt "%1 is Simon version"
msgid "Sam: part of Simon %1\n"
msgstr "Sam: en del av Simon %1\n"

#: exporttestresults.cpp:210
#, kde-format
msgctxt "%1 is filename"
msgid ""
"Could not initialize output template at \"%1\". Please select a valid output "
"format."
msgstr "Kunde inte initiera utdatamall \"%1\". Välj ett giltigt utdataformat."

#: exporttestresults.cpp:221
#, kde-format
msgctxt "%1 is output type"
msgid ""
"Output type not supported: %1.\n"
"\n"
"Please make sure that your output format is compatible with this version of "
"Sam."
msgstr ""
"Utdatatypen stöds inte: %1.\n"
"\n"
"Försäkra dig om att utdataformatet är kompatibelt med den här versionen av "
"Sam."

#: exporttestresults.cpp:240
#, kde-format
msgctxt "%1 is error message"
msgid "Failed to parse template: %1"
msgstr "Misslyckades tolka mall: %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: exporttestresultsdlg.ui:21 main.ui:569
msgid "General"
msgstr "Allmänt"

#. i18n: ectx: property (text), widget (QLabel, label)
#: exporttestresultsdlg.ui:30
msgid "Title:"
msgstr "Rubrik:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: exporttestresultsdlg.ui:50
msgid "Task definition:"
msgstr "Aktivitetsdefinition:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: exporttestresultsdlg.ui:64
msgid "Output format:"
msgstr "Utdataformat:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: exporttestresultsdlg.ui:74
msgid "Options:"
msgstr "Alternativ:"

#. i18n: ectx: property (text), widget (QCheckBox, cbTables)
#: exporttestresultsdlg.ui:84
msgid "Tables"
msgstr "Tabeller"

#. i18n: ectx: property (text), widget (QCheckBox, cbGraphs)
#: exporttestresultsdlg.ui:94
msgid "Graphs"
msgstr "Diagram"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: exporttestresultsdlg.ui:101
msgid "Conclusion:"
msgstr "Avslutning:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_9)
#: exporttestresultsdlg.ui:119
msgid "Experiment"
msgstr "Experiment"

#. i18n: ectx: property (text), widget (QLabel, label_29)
#: exporttestresultsdlg.ui:135
msgid "Short description:"
msgstr "Kort beskrivning:"

#. i18n: ectx: property (text), widget (QLabel, label_30)
#: exporttestresultsdlg.ui:149
msgid "Conducted on:"
msgstr "Utfört:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_8)
#: exporttestresultsdlg.ui:164
msgid "System"
msgstr "System"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: exporttestresultsdlg.ui:170
msgid "System definition:"
msgstr "Systemdefinition:"

#. i18n: ectx: property (text), widget (QCheckBox, cbDetailedSystemInformation)
#: exporttestresultsdlg.ui:186
msgid "Detailed"
msgstr "Detaljerad"

#. i18n: ectx: property (text), widget (KPushButton, pbRetrieveSystemInformation)
#: exporttestresultsdlg.ui:193
msgid "Retrieve system information"
msgstr "Hämta systeminformation"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: exporttestresultsdlg.ui:213
msgid "Vocabulary"
msgstr "Ordförråd"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: exporttestresultsdlg.ui:243
msgid "Number of words:"
msgstr "Antal ord:"

#. i18n: ectx: property (text), widget (QLabel, label_12)
#: exporttestresultsdlg.ui:264
msgid "Number of pronunciations:"
msgstr "Antal uttal:"

#. i18n: ectx: property (text), widget (QLabel, lbSphinxGrammar)
#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: exporttestresultsdlg.ui:272 sphinxtestconfigurationwidget.ui:84
msgid "Grammar"
msgstr "Grammatik..."

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: exporttestresultsdlg.ui:303
msgid "Trainingssamples"
msgstr "Övningssamplingar"

#. i18n: ectx: attribute (title), widget (QWidget, tab_5)
#: exporttestresultsdlg.ui:317
msgid "Testset"
msgstr "Provuppsättning"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_6)
#: juliustestconfigurationwidget.ui:49
msgid "HTK input files"
msgstr "HTK-indatafiler"

#. i18n: ectx: property (text), widget (QLabel, lbHmmdefs)
#: juliustestconfigurationwidget.ui:61
msgid "HMM definitions:"
msgstr "HMM-definitioner:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_5)
#: juliustestconfigurationwidget.ui:129 sphinxtestconfigurationwidget.ui:100
msgctxt "Test the model"
msgid "Test configuration"
msgstr "Provinställning"

#. i18n: ectx: property (text), widget (QLabel, lbTestPrompts)
#. i18n: ectx: property (text), widget (QLabel, lbTestPrompts_3)
#: juliustestconfigurationwidget.ui:141 sphinxtestconfigurationwidget.ui:112
msgid "Test-prompts:"
msgstr "Provfrågor:"

#. i18n: ectx: property (text), widget (QLabel, lbTestPromptsBasePath)
#. i18n: ectx: property (text), widget (QLabel, lbTestPromptsBasePath_3)
#: juliustestconfigurationwidget.ui:154 sphinxtestconfigurationwidget.ui:125
msgid "Test-prompts base path:"
msgstr "Bassökväg för provfrågor:"

#. i18n: ectx: property (text), widget (QLabel, lbSampleRate)
#: juliustestconfigurationwidget.ui:180 main.ui:246
#: sphinxtestconfigurationwidget.ui:138
msgid "Samplerate:"
msgstr "Samplingsfrekvens:"

#. i18n: ectx: property (text), widget (KPushButton, pbRemove)
#: juliustestconfigurationwidget.ui:221 sphinxtestconfigurationwidget.ui:179
msgid "Remove"
msgstr "Ta bort"

#: latexreporttemplateengine.cpp:70
msgid "LaTeX files (zipped) *.zip"
msgstr "Latex-filer (komprimerade) *.zip"

#: main.cpp:28
msgid "An acoustic model modeller"
msgstr "Modellering av akustiska modeller"

#: main.cpp:32
msgid "Sam"
msgstr "Sam"

#: main.cpp:33
msgid "(C) 2009 Peter Grasch"
msgstr "© 2011 Peter Grasch"

#: main.cpp:34
msgid "Peter Grasch"
msgstr "Peter Grasch"

#: main.cpp:38
msgid "Select model type (\"sphinx\" or \"htk\")"
msgstr "Välj modelltyp (\"sphinx\" eller \"htk\")"

#: main.cpp:39
msgid "Get configuration from Simon"
msgstr "Hämta inställning från Simon"

#: main.cpp:40
msgid "Automatically start compiling the model"
msgstr "Starta automatiskt kompilera modellen"

#: main.cpp:41
msgid "Automatically start testing the model(s)"
msgstr "Starta automatiskt modellprov"

#: main.cpp:42
msgid "Export test results to the given filename"
msgstr "Exportera provresultat till angivet filnamn"

#: main.cpp:43
msgid "Batch processing: Close after all other arguments have been processed"
msgstr "Bakgrundsbehandling: Stäng efter alla andra väljare har behandlats"

#: main.cpp:44
msgid "Store compilation log"
msgstr "Lagra kompileringslogg"

#: main.cpp:45
msgid "Write opened file to disk when done (batch mode)."
msgstr "Skriv öppnad fil till disk när klar (bakgrundsläge)."

#: main.cpp:47
msgid "If provided, this file will automatically be loaded"
msgstr "Om angiven, kommer filen automatiskt att läsas in"

#. i18n: ectx: property (windowTitle), widget (QMainWindow, MainWindow)
#: main.ui:20
msgid "sam - Untitled"
msgstr "Sam - Namnlös"

#. i18n: ectx: attribute (title), widget (QWidget, Seite_3)
#: main.ui:31
msgid "Input && output files"
msgstr "Indata- och utdatafiler"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: main.ui:37
msgid "Type:"
msgstr "Typ:"

#. i18n: ectx: property (text), item, widget (QComboBox, cbType)
#: main.ui:48
msgid "CMU Sphinx"
msgstr "CMU Sphinx"

#. i18n: ectx: property (text), item, widget (QComboBox, cbType)
#: main.ui:53
msgid "HTK, Julius"
msgstr "HTK, Julius"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: main.ui:65
msgid "Creating"
msgstr "Skapar"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: main.ui:71
msgid "Input"
msgstr "Indata"

#. i18n: ectx: property (text), widget (KPushButton, pbSerializeScenarios)
#: main.ui:80
msgid "Serialize scenarios"
msgstr "Serialisera scenarier"

#. i18n: ectx: property (text), widget (KPushButton, pbImportRecognitionSamples)
#: main.ui:87
msgid "Import recognition samples"
msgstr "Importera igenkänningssamplingar"

#. i18n: ectx: property (text), widget (KPushButton, pbSerializePrompts)
#: main.ui:94
msgid "Serialize prompts"
msgstr "Serialisera frågor"

#. i18n: ectx: property (text), widget (QLabel, lbDict)
#: main.ui:108
msgid "Directory:"
msgstr "Katalog:"

#. i18n: ectx: property (text), widget (QLabel, lbPhone)
#: main.ui:134
msgid "Model name:"
msgstr "Modellnamn:"

#. i18n: ectx: property (text), widget (QLabel, lbLexicon)
#: main.ui:167
msgid "Lexicon:"
msgstr "Ordbok:"

#. i18n: ectx: property (text), widget (QLabel, lbGrammar)
#: main.ui:180
msgid "Grammar:"
msgstr "Grammatik:"

#. i18n: ectx: property (text), widget (QLabel, lbVocabulary)
#: main.ui:193
msgid "Vocabulary:"
msgstr "Ordförråd:"

#. i18n: ectx: property (text), widget (QLabel, lbPrompts)
#: main.ui:206
msgid "Prompts:"
msgstr "Frågor:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: main.ui:219
msgid "Script base prefix:"
msgstr "Basprefix för skript:"

#. i18n: ectx: property (text), widget (QLabel, lbPromptsBasePath)
#: main.ui:233
msgid "Prompts base path:"
msgstr "Bassökväg för frågor:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: main.ui:269
msgid "Base model:"
msgstr "Basmodell:"

#. i18n: ectx: property (filter), widget (KUrlRequester, urBaseModel)
#: main.ui:276
msgid "*.sbm"
msgstr "*.sbm"

#. i18n: ectx: property (text), widget (KPushButton, pbCreate)
#: main.ui:283
msgid "Create base model from model files"
msgstr "Skapa basmodell från modellfiler"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: main.ui:293
msgid "Output"
msgstr "Utdata"

#. i18n: ectx: property (text), widget (QRadioButton, rbStaticModel)
#: main.ui:302
msgid "Static model"
msgstr "Statisk modell"

#. i18n: ectx: property (text), widget (QRadioButton, rbAdaptedBaseModel)
#: main.ui:309
msgid "Adapted base model"
msgstr "Anpassad basmodell"

#. i18n: ectx: property (text), widget (QRadioButton, rbDynamicModel)
#: main.ui:316
msgid "Entirely user-generated model"
msgstr "Helt användargenererad modell"

#. i18n: ectx: property (text), widget (QLabel, lbHmmdefs)
#: main.ui:326
msgid "Output model:"
msgstr "Utdatamodell:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: main.ui:343
msgid "Testing"
msgstr "Provar"

#. i18n: ectx: property (text), widget (KPushButton, pbExtractSimonModel)
#: main.ui:373
msgid "Extract Simon Model"
msgstr "Extrahera Simon-modell"

#. i18n: ectx: property (text), widget (KPushButton, pbAddTestConfiguration)
#: main.ui:393
msgid "Add"
msgstr "Lägg till"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: main.ui:407
msgid "Adapt scenarios"
msgstr "Anpassa scenarier"

#. i18n: ectx: property (text), widget (QLabel, lbAdaptLog)
#: main.ui:429
msgid "Adapt log:"
msgstr "Anpassningslogg:"

#. i18n: ectx: attribute (title), widget (QWidget, Seite)
#: main.ui:446
msgid "Create model"
msgstr "Skapa modell"

#. i18n: ectx: property (text), widget (KPushButton, pbCompileModel)
#: main.ui:454 samview.cpp:113
msgid "Build model"
msgstr "Bygg modell"

#. i18n: ectx: property (text), widget (KPushButton, pbCancelBuildModel)
#. i18n: ectx: property (text), widget (KPushButton, pbCancelTestModel)
#: main.ui:471 main.ui:534
msgid "Cancel"
msgstr "Avbryt"

#. i18n: ectx: property (text), widget (QLabel, lbBuildLog)
#: main.ui:482
msgid "Build log:"
msgstr "Bygglogg:"

#. i18n: ectx: property (text), widget (KPushButton, pbSaveCompleteBuildlog)
#: main.ui:499
msgid "Save complete buildlog (HTML)"
msgstr "Spara fullständig bygglogg (HTML)"

#. i18n: ectx: attribute (title), widget (QWidget, Seite_2)
#. i18n: ectx: property (text), widget (KPushButton, pbTestModel)
#: main.ui:509 main.ui:517 samview.cpp:122
msgid "Test model"
msgstr "Prova modell"

#. i18n: ectx: property (text), widget (QLabel, lbTestLog)
#: main.ui:545
msgid "Test log:"
msgstr "Provlogg:"

#. i18n: ectx: attribute (title), widget (QWidget, Seite_4)
#: main.ui:562 samview.cpp:130
msgid "Test results"
msgstr "Provresultat"

#: reporttemplateengine.cpp:174
msgid "Overview"
msgstr "Översikt"

#: reporttemplateengine.cpp:243
msgid "Could not clean up temporary folder."
msgstr "Kunde inte rensa tillfällig katalog."

#: reporttemplateengine.cpp:259
msgid "Failed to store file."
msgstr "Misslyckades lagra fil."

#. i18n: ectx: ToolBar (mainToolBar)
#: samui.rc:5
msgid "Main Actions"
msgstr "Huvudåtgärder"

#. i18n: ectx: Menu (build)
#: samui.rc:17
msgid "Build"
msgstr "Bygg"

#. i18n: ectx: Menu (test)
#: samui.rc:23
msgctxt "Test the model"
msgid "Test"
msgstr "Prov"

#: samview.cpp:105
msgid "Modify Simon's model"
msgstr "Ändra Simons modell"

#: samview.cpp:106
msgid "Manage Simon's current model with SSC"
msgstr "Hantera Simons nuvarande modell med SSC"

#: samview.cpp:114
msgid "Build the currently open model."
msgstr "Bygg modellen som för närvarande är öppen."

#: samview.cpp:123
msgid "Test the model."
msgstr "Prova modellen."

#: samview.cpp:131
msgid "Display the test results."
msgstr "Visa provresultatet."

#: samview.cpp:138
msgid "Export test result"
msgstr "Exportera provresultat"

#: samview.cpp:139
msgid "Export the test results to a file."
msgstr "Exportera provresultatet till en fil."

#: samview.cpp:191
msgid "SAM was built without SPHINX support."
msgstr "SAM byggdes utan stöd för SPHINX."

#: samview.cpp:250
msgid "Open Simon Model"
msgstr "Öppna Simon-modell"

#: samview.cpp:252
msgid "Output directory"
msgstr "Utdatakatalog"

#: samview.cpp:259
msgid "Failed to unpack Simon base model."
msgstr "Misslyckades packa upp Simon-basmodell"

#: samview.cpp:282
msgid ""
"Your Sam configuration has changed.\n"
"\n"
"Do you want to save?"
msgstr ""
"Inställningen av Sam har ändrats.\n"
"\n"
"Vill du spara den?"

#: samview.cpp:300
msgid "No current log."
msgstr "Ingen aktuell logg."

#: samview.cpp:310
msgid "HTML files *.html"
msgstr "HTML-filer *.html"

#: samview.cpp:315
#, kde-format
msgctxt "%1 is the file name"
msgid "Could not open output file %1."
msgstr "Kunde inte öppna utdatafil %1."

#: samview.cpp:407
#, kde-format
msgctxt "Tag of the test"
msgid "Test started: %1"
msgstr "Prov startat: %1"

#: samview.cpp:416
#, kde-format
msgctxt "Tag of the test"
msgid "Test aborted: %1"
msgstr "Prov avbrutet: %1"

#: samview.cpp:424
#, kde-format
msgctxt "Tag of the test"
msgid "Test completed: %1"
msgstr "Prov färdigt: %1"

#: samview.cpp:456
#, kde-format
msgid "Ran 1 test."
msgid_plural "Ran %1 tests."
msgstr[0] "Utförde 1 prov."
msgstr[1] "Utförde %1 prov."

#: samview.cpp:607 samview.cpp:635
msgid "Sam projects *.sam"
msgstr "Sam-projekt *.sam"

#: samview.cpp:649
msgid "Untitled"
msgstr "Namnlös"

#: samview.cpp:651
#, kde-format
msgctxt "%1 is file name"
msgid "Sam - %1 [*]"
msgstr "Sam - %1 [*]"

#: samview.cpp:661 samview.cpp:738
#, kde-format
msgctxt "%1 is file name"
msgid "Cannot open file: %1"
msgstr "Kan inte öppna fil: %1"

#: samview.cpp:670
msgid "Corrupt or outdated Sam configuration file."
msgstr "Felaktig eller föråldrad Sam-inställningsfil."

#: samview.cpp:888
msgid ""
"You now have to provide a (preferably empty) folder where you want to "
"serialize the scenarios to"
msgstr ""
"Nu måste du ange en katalog (helst tom) där du vill att scenarier ska "
"serialiseras"

#: samview.cpp:888
msgid "Do not ask again"
msgstr "Fråga inte igen"

#: samview.cpp:889
msgid "Serialized scenario output"
msgstr "Serialiserad scenarieutdata"

#: samview.cpp:905
msgid "Open simon prompts"
msgstr "Öppna Simon-frågor"

#: samview.cpp:973
#, kde-format
msgctxt "%1 is scenario id"
msgid "Could not find scenario: %1"
msgstr "Kunde inte hitta scenario: %1"

#: samview.cpp:1007
msgid "Unknown model type"
msgstr "Okänd modelltyp"

#: samview.cpp:1019
#, kde-format
msgctxt "%1 is path to the base model"
msgid "Could not open base model at \"%1\"."
msgstr "Kunde inte öppna basmodell \"%1\"."

#: samview.cpp:1173
msgctxt ""
"The tag name of an automatically added test set. The needed string really is "
"please change this (for the user to change)."
msgid "PLEASE_CHANGE_THIS"
msgstr "ÄNDRA HÄR"

#: samview.cpp:1183
msgid "Adaption aborted"
msgstr "Anpassning avbruten"

#: samview.cpp:1198
#, kde-format
msgctxt "%1 is error message"
msgid ""
"Failed to adapt model:\n"
"\n"
"%1"
msgstr ""
"Misslyckades anpassa modell:\n"
"\n"
"%1"

#: samview.cpp:1230
#, kde-format
msgid "Grammar class undefined: %1"
msgstr "Grammatikklass odefinierad: %1"

#: samview.cpp:1237
#, kde-format
msgid "Word undefined: %1"
msgstr "Ord odefinierat: %1"

#: samview.cpp:1244
#, kde-format
msgid "Phoneme undefined: %1"
msgstr "Fonem odefinierat: %1"

#: samview.cpp:1260
msgid ""
"Build log too big to display. Please use the button below to store it to a "
"local file."
msgstr ""
"Bygglogg för stor för att visa. Använd knappen nedan för att lagra den i en "
"lokal fil."

#: samview.cpp:1325
msgid ""
"No tests configured yet. Please provide your test configuration in the "
"input / output section."
msgstr ""
"Inget prov inställt ännu. Tillhandahåll provinställningen i avdelningen för "
"in- och utdata."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: sphinxtestconfigurationwidget.ui:46
msgid "Sphinx input files"
msgstr "Sphinx-indatafiler"

#. i18n: ectx: property (text), widget (QLabel, lbModelDir)
#: sphinxtestconfigurationwidget.ui:58
msgid "Model directory:"
msgstr "Modellordlista:"

#. i18n: ectx: property (text), widget (QLabel, lbSphinxDict)
#: sphinxtestconfigurationwidget.ui:71
msgid "Dictionary:"
msgstr "Ordlista:"

#. i18n: ectx: property (text), widget (QLabel, lbRecognitionRate)
#: testresult.ui:22
msgid "Overall recognition rate (confidence):"
msgstr "Övergripande igenkänningsfrekvens (konfidens):"

#. i18n: ectx: attribute (title), widget (QWidget, pgSentenceResults)
#: testresult.ui:48
msgid "Sentences"
msgstr "Meningar"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: testresult.ui:65
msgid "Recognized words"
msgstr "Igenkända ord"

#. i18n: ectx: attribute (title), widget (QWidget, pgFileResults)
#: testresult.ui:82
msgid "Files"
msgstr "Filer"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: testresult.ui:90
msgid "Find file:"
msgstr "Sök fil:"

#. i18n: ectx: property (clickMessage), widget (KLineEdit, leResultFilesFilter)
#: testresult.ui:100
msgid "Filter"
msgstr "Filter"

#. i18n: ectx: property (text), widget (KPushButton, pbEditSample)
#: testresult.ui:130
msgid "Edit sample"
msgstr "Redigera sampling"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: testresult.ui:140
msgid "Log"
msgstr "Logg"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: testresult.ui:169
msgid "Accuracy:"
msgstr "Noggrannhet:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: testresult.ui:183
msgid "Word error rate:"
msgstr "Ordfelfrekvens:"

#: testresultplotter.cpp:53 testresultplotter.cpp:64
msgid "Accuracy"
msgstr "Noggrannhet"

#: testresultplotter.cpp:56 testresultplotter.cpp:65
msgid "Confidence"
msgstr "Konfidens"

#: testresultwidget.cpp:155
msgid "Modify sample"
msgstr "Ändra sampling"

#: testresultwidget.cpp:167 testresultwidget.cpp:192
msgid "Could not modify prompts file"
msgstr "Kunde inte ändra frågefil"

#: testresultwidget.cpp:179
msgid "Could not overwrite prompts file"
msgstr "Kunde inte skriva över frågefil"

#: testresultwidget.cpp:183 testresultwidget.cpp:199
#, kde-format
msgctxt "%1 is file name"
msgid "Could not remove original sample:  %1."
msgstr "Kunde inte ta bort originalsampling: %1."

#: testresultwidget.cpp:204
#, kde-format
msgctxt "%1 is source file name, %2 is destination file name"
msgid "Could not copy sample from temporary path %1 to %2."
msgstr "Kunde inte kopiera sampling från tillfällig sökväg %1 till %2."

#: testresultwidget.cpp:228
#, kde-format
msgid ""
"Result %1 of %2\n"
"=====================\n"
msgstr ""
"Resultat %1 av %2\n"
"=======================\n"